
$(document).ready(function () {
  $("#btn-check").on("click", function () {
    onBtnCheckVoucherClick();
  })
})
"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gREADY_STATE_REQUEST_DONE = 4;
var gSTATUS_REQUEST_DONE = 200;

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm này để xử lý sự kiện nút check voucher click
function onBtnCheckVoucherClick() {


  var vVoucherOjb = {
    maGiamGia: ""
  }
  // B1: lấy giá trị nhập trên form, mã voucher (Thu thập dữ liệu)
  readData(vVoucherOjb)
  // B2: Validate data
  var vIsCheck = validateData(vVoucherOjb);
  if (vIsCheck) {
    // B3: Tạo request và gửi mã voucher về server
    var bXmlHttp = new XMLHttpRequest();
    sendVoucherToServer(vVoucherOjb, bXmlHttp);
    // B4: xu ly response khi server trả về
    bXmlHttp.onreadystatechange = function () {
      if (bXmlHttp.readyState === gREADY_STATE_REQUEST_DONE && bXmlHttp.status === gSTATUS_REQUEST_DONE) {
        
        processResponse(bXmlHttp);

      } else if(bXmlHttp.readyState === gREADY_STATE_REQUEST_DONE ){
        var vResultCheckElement = $("#div-result-check");
        vResultCheckElement.html ("Mã giảm giá " + vVoucherOjb.maGiamGia + " Kkông tồn tại mã giảm giá");
      }
    }
  }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function readData(paramVoucher) {
  var vVoucherCode = $("#voucher").val();
  paramVoucher.maGiamGia = vVoucherCode
}
// Hàm này dùng để validate data
// input: ma voucher nguoi dung nhap vao
// output: tra ve true neu ma voucher duoc nhap, nguoc lai thi return false
function validateData(paramVoucher) {
  var vResultCheckElement = $("#div-result-check");
  if (paramVoucher.maGiamGia === "") {
    // Hiển thị câu thông báo lỗi ra
    vResultCheckElement.html("Mã giảm giá chưa nhập!")
    // Thay đổi class css, để đổi màu chữ thành màu đỏ
    vResultCheckElement.prop("class", "text-danger")
    return false;
  } else {

    // Thay đổi class css, để đổi màu chữ thành màu đen bình thường
    vResultCheckElement.prop("class", "text-dark");
    vResultCheckElement.html("")
    return true;
  }
}

// Ham thuc hien viec call api va gui ma voucher ve server
function sendVoucherToServer(paramVoucher, paramXmlHttp) {
  paramXmlHttp.open("GET", "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/" + paramVoucher.maGiamGia, true);
  paramXmlHttp.send();
}

// Hàm này được dùng để xử lý khi server trả về response
function processResponse(paramXmlHttp) {

  // B2: Parsing chuỗi JSON thành Object
  var vVoucherResObj = JSON.parse(paramXmlHttp.responseText);
  console.log(vVoucherResObj);
  // B3: xử lý mã giảm giá dựa vào đối tượng vừa có
  var vDiscount = vVoucherResObj.phanTramGiamGia;
  var vResultCheckElement = $("#div-result-check");
  vResultCheckElement.html("Mã giảm giá " + vDiscount + "%");
  
}